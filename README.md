# laravel-echo聊天室
基于laravel5.5和redis的聊天室demo    
克隆到本地命令行输入：`composer install`    
再输入 `cp .env.example .env`    
再输入`npm install -g laravel-echo-server`
修改.env 中的    
![.env](https://git.oschina.net/uploads/images/2017/1005/110320_7cb8e5fa_1285229.png "{2ROQD6UT{[$[EDEKLCCVOJ.png")    
命令行输入`laravel-echo-server init`,输入这条命令前确保先运行redis服务端    
最后开两个命令行窗口，执行两条命令：    
`laravel-echo-server start`    
`php artisan queue:work`

