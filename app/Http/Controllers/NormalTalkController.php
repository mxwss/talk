<?php

namespace App\Http\Controllers;

use App\Events\SendMessage;
use Illuminate\Http\Request;

class NormalTalkController extends Controller
{
    public function index()
    {
        return view('normal.index');
    }

    public function add(Request $request)
    {
        $message = $request->validate(['message'=>'required']);
        broadcast(new SendMessage($message['message']))->toOthers();
        return response()->json(['status'=>'success']);
    }
}
