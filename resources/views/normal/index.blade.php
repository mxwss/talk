<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>weTalk</title>

    <!-- Fonts -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('vendor/dist/css/AdminLTE.min.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/dist/css/skins/_all-skins.min.css')}}">
    <!-- Styles -->
</head>
<body>
<div class="content">
    <div class="row">
    <div class="box box-warning direct-chat direct-chat-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Direct Chat</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- Conversations are loaded here -->
            <div class="direct-chat-messages">
                <!-- Message. Default to the left -->


            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="hx">
                <div class="input-group">
                    <input type="text" name="message" placeholder="在这里输入内容" class="form-control">
                    <span class="input-group-btn">
                            <button type="button" class="btn btn-warning btn-flat save">发送</button>
                          </span>
                </div>
            </div>
        </div>
        <!-- /.box-footer-->
    </div>
    </div>
</div>
<script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script>
    $(".hx .save").click(function () {
        $.ajax({
            url:'/normal/add',
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'X-Socket-ID':Echo.socketId()
            },
            data:{
                message:$(".hx input[name=message]").val()
            },
            dataType:'json',
            success:function (res) {
                if(res.status=='success'){
                    $(".direct-chat-messages").append("<div class=\"direct-chat-msg right\">\n" +
                        "                    <div class=\"direct-chat-info clearfix\">\n" +
                        "                        <span class=\"direct-chat-name pull-right\">"+Echo.socketId()+"</span>\n" +
                        "                        <span class=\"direct-chat-timestamp pull-left\">"+"{{date('H:i',time())}}"+
                        "                    </div>\n" +
                        "                    <div class=\"direct-chat-text\">\n" +
                        $(".hx input[name=message]").val() +
                        "                    </div>\n" +
                        "                </div>");
                }
            }
        })
    });
    if(typeof io==='undefined'){
        alert('请检查你的laravel-echo服务器');
    }else {
        Echo.channel('everyone')
            .listen('SendMessage', function (e) {
                 if(e.message){
                     $(".direct-chat-messages").append("<div class=\"direct-chat-msg\">\n" +
                         "                    <div class=\"direct-chat-info clearfix\">\n" +
                         "                        <span class=\"direct-chat-name pull-left\">"+Echo.socketId()+"</span>\n" +
                         "                        <span class=\"direct-chat-timestamp pull-right\">" +"{{date('H:i',time())}}"+
                         "                    </div>\n" +
                         "                    <div class=\"direct-chat-text\">\n" +
                         e.message +
                         "                    </div>\n" +
                         "                </div>");
                 }
            });
    }
</script>
</body>
</html>
